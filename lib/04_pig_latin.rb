def translate(str)
  arr = str.split(" ")
  arr.each_with_index do |el, idx|
    if beginning_with_vowel?(el)
      arr[idx] = el + "ay"
    else
      arr[idx] = return_the_new_word(el) + "ay"
    end
  end
  arr.join(" ")
end

def beginning_with_vowel?(word)
  vowel = "aeiou"
  if vowel.include?(word[0])
    true
  else
    false
  end
end

def return_the_new_word(word)
  idx = identify_the_first_vowel(word)
  arr = word.split("")
  new_arr = arr[idx..-1] + arr[0..(idx-1)]
  new_arr.join
end

def identify_the_first_vowel(word)
  vowel_other_than_u = "aeio"
  arr = word.split("")
  arr.each_with_index do |el, idx|
    if vowel_other_than_u.include?(el) || (el == "u" && arr[idx-1] != "q")
      return idx
    end
  end
end
