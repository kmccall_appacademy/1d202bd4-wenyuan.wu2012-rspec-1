def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, num = 2)
  new_sub_str = str + " "
  new_str = new_sub_str * (num-1)
  new_str << str
end

def start_of_word(str, num)
  str[0..(num-1)]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  arr = str.split(" ").map(&:capitalize)
  idx = 1
  while idx < arr.length
    if arr[idx]== "And" or arr[idx] == "Over" or arr[idx] == "The"
      arr[idx] = arr[idx].downcase
    end
    idx +=1
  end
  arr.join(" ")
end
